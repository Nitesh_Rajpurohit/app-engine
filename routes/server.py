import webapp2

routes = webapp2.WSGIApplication([

    ('/', 'app.controllers.main.MainPage'),
    ('/sign', 'app.controllers.main.Guestbook'),
    ('/sample_Email','app.workers.emailer.SendEmail'),
	('/excel','app.workers.emailer.ExcelGen'),
	('/reports','app.workers.dashboard(external).Reporter'),
    ('/reports/edit/([^/]+)','app.workers.dashboard.EditReport'),
    ('/AddTab', 'app.controllers.main.Addtab'),
    ('/DisplayTab', 'app.controllers.main.DisplayTab'),
    ('/DisplayTab/UpdateTab', 'app.controllers.main.DisplayTab')

    # Coming Soon

    # Client Group
    # [Client]
        # = List Modules
        #
    # [Client]/[ReportName]/[Subscribers]
        # = add up to 3 emails (use distribution list for larger sets)
    # [Client]/[ReportName]/[Schedule]
        # = View the next time a report is set to run. / link put in request for new schedule

    # ('/Michaels', 'app.controllers.Michaels.ShowAllReports'),
        # GET see all reports availble
        # POST invalid
        # PUT invalid
        # DELETE invalid

    # ('/Michaels/Report1/subscribers', 'app.controllers.Michaels.ExcelFile'),
        # GET see list of subscribers
        # POST add subscriber
        # PUT edit subscriber email address
        # DELETE remove subscriber

    # ('/Michaels/Report1/subscribers', 'app.controllers.Michaels.ExcelFile'),
        # GET see list of subscribers
        # POST add subscriber
        # PUT edit subscriber email address
        # DELETE remove subscriber


    # ('/7Eleven', 'app.controllers.KM.ShowAllReports'),

    # ('/Outback', 'app.controllers.KM.ShowAllReports'),

    # ('/Outback', 'app.controllers.KM.ShowAllReports'),

# Coming Even Later
# OLAP Cube - Stores, Offers, Consumers
#Offers
    # /api/v1/[ClientID]/offers/weekly.json?    dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3]    &sig=HMAC(url+timestamp(MM), AUTHKEY)
    # /api/v1/[ClientID]/offers/weekly.xml?     dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3]    &sig=HMAC(url+timestamp(MM), AUTHKEY)
    # /api/v1/[ClientID]/offers/weekly.png?     dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3] &imgHeight=H &ImgWidth=W &ChartStyle=barGraph &sig=HMAC(url+timestamp(MM), AUTHKEY)

    # /api/v1/[ClientID]/offers/monthly.json?   dtStart=X &dtEnd=Y &OfferFilter=[include/exclude,1,2,3]  &sig=HMAC(url+timestamp(MM), AUTHKEY)
    # /api/v1/[ClientID]/offers/monthly.xml?    dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3]   &sig=HMAC(url+timestamp(MM), AUTHKEY)
    # /api/v1/[ClientID]/offers/monthly.png?    dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3] &imgHeight=H &ImgWidth=W &ChartStyle=barGraph &sig=HMAC(url+timestamp(MM), AUTHKEY)

#users
    # /api/v1/[ClientID]/users/benchmarks.json?     dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3]    &sig=HMAC(url+timestamp(MM), AUTHKEY)
    # /api/v1/[ClientID]/users/benchmarks.xml?      dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3]    &sig=HMAC(url+timestamp(MM), AUTHKEY)
    # /api/v1/[ClientID]/users/benchmarks.png?      dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3] &imgHeight=H &ImgWidth=W &ChartStyle=barGraph &sig=HMAC(url+timestamp(MM), AUTHKEY)

    # /api/v1/[ClientID]/users/repeaters.json?     dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3]    &sig=HMAC(url+timestamp(MM), AUTHKEY)
    # /api/v1/[ClientID]/users/repeaters.xml?      dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3]    &sig=HMAC(url+timestamp(MM), AUTHKEY)
    # /api/v1/[ClientID]/users/repeaters.png?      dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3] &imgHeight=H &ImgWidth=W &ChartStyle=barGraph &sig=HMAC(url+timestamp(MM), AUTHKEY)

    # /api/v1/[ClientID]/users/weekly.json?     dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3]    &sig=HMAC(url+timestamp(MM), AUTHKEY)
    # /api/v1/[ClientID]/users/weekly.xml?      dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3]    &sig=HMAC(url+timestamp(MM), AUTHKEY)
    # /api/v1/[ClientID]/users/weekly.png?      dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3] &imgHeight=H &ImgWidth=W &ChartStyle=barGraph &sig=HMAC(url+timestamp(MM), AUTHKEY)

    # /api/v1/[ClientID]/users/monthly.json?    dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3]    &sig=HMAC(url+timestamp(MM), AUTHKEY)
    # /api/v1/[ClientID]/users/monthly.xml?     dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3]    &sig=HMAC(url+timestamp(MM), AUTHKEY)
    # /api/v1/[ClientID]/users/monthly.png?     dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3] &imgHeight=H &ImgWidth=W &ChartStyle=barGraph &sig=HMAC(url+timestamp(MM), AUTHKEY)

#Stores
    # /api/v1/[ClientID]/stores/weekly.json?    dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3]    &sig=HMAC(url+timestamp(MM), AUTHKEY)
    # /api/v1/[ClientID]/stores/weekly.xml?     dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3]    &sig=HMAC(url+timestamp(MM), AUTHKEY)
    # /api/v1/[ClientID]/stores/weekly.png?     dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3] &imgHeight=H &ImgWidth=W &ChartStyle=barGraph &sig=HMAC(url+timestamp(MM), AUTHKEY)

    # /api/v1/[ClientID]/stores/monthly.json?   dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3]    &sig=HMAC(url+timestamp(MM), AUTHKEY)
    # /api/v1/[ClientID]/stores/monthly.xml?    dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3]    &sig=HMAC(url+timestamp(MM), AUTHKEY)
    # /api/v1/[ClientID]/stores/monthly.png?    dtStart=X &dtEnd=Y &OfferFilter=[include/exclude1,2,3] &imgHeight=H &ImgWidth=W &ChartStyle=barGraph &sig=HMAC(url+timestamp(MM), AUTHKEY)

#Predictions
    # /api/v1/[ClientID]/predict/TopOffers?          &limit=3 &users =[X,Y,Z] output==> [1,2,3],[4,5,6],[7,8,9]
    # /api/v1/[ClientID]/predict/TopOffers.nnet?     &limit=3 &users =[X,Y,Z] output==> [1,2,3],[4,5,6],[7,8,9]
    # /api/v1/[ClientID]/predict/TopOffers.knn?      &limit=3 &users =[X,Y,Z] output==> [1,2,3],[4,5,6],[7,8,9]
    # /api/v1/[ClientID]/predict/TopOffers.c45tree?  &limit=3 &users =[X,Y,Z] output==> [1,2,3],[4,5,6],[7,8,9]
    # /api/v1/[ClientID]/predict/TopOffers.id3tree?  &limit=3 &users =[X,Y,Z] output==> [1,2,3],[4,5,6],[7,8,9]
    # /api/v1/[ClientID]/predict/TopOffers.forrest?  &limit=3 &users =[X,Y,Z] output==> [1,2,3],[4,5,6],[7,8,9]
    # /api/v1/[ClientID]/predict/TopOffers.google?   &limit=3 &users =[X,Y,Z] output==> [1,2,3],[4,5,6],[7,8,9]

    # /api/v1/[ClientID]/predict/TopUsers?          &limit=3 &users =[X,Y,Z] output==> [1,2,3],[4,5,6],[7,8,9]
    # /api/v1/[ClientID]/predict/TopUsers.nnet?     &limit=3 &users =[X,Y,Z] output==> [1,2,3],[4,5,6],[7,8,9]
    # /api/v1/[ClientID]/predict/TopUsers.knn?      &limit=3 &users =[X,Y,Z] output==> [1,2,3],[4,5,6],[7,8,9]
    # /api/v1/[ClientID]/predict/TopUsers.c45tree?  &limit=3 &users =[X,Y,Z] output==> [1,2,3],[4,5,6],[7,8,9]
    # /api/v1/[ClientID]/predict/TopUsers.id3tree?  &limit=3 &users =[X,Y,Z] output==> [1,2,3],[4,5,6],[7,8,9]
    # /api/v1/[ClientID]/predict/TopUsers.forrest?  &limit=3 &users =[X,Y,Z] output==> [1,2,3],[4,5,6],[7,8,9]
    # /api/v1/[ClientID]/predict/TopUsers.google?   &limit=3 &users =[X,Y,Z] output==> [1,2,3],[4,5,6],[7,8,9]

    # /api/v1/[ClientID]/predict/TopStores?          &limit=3 &users =[X,Y,Z] output==> [1,2,3],[4,5,6],[7,8,9]
    # /api/v1/[ClientID]/predict/TopStores.nnet?     &limit=3 &users =[X,Y,Z] output==> [1,2,3],[4,5,6],[7,8,9]
    # /api/v1/[ClientID]/predict/TopStores.knn?      &limit=3 &users =[X,Y,Z] output==> [1,2,3],[4,5,6],[7,8,9]
    # /api/v1/[ClientID]/predict/TopStores.c45tree?  &limit=3 &users =[X,Y,Z] output==> [1,2,3],[4,5,6],[7,8,9]
    # /api/v1/[ClientID]/predict/TopStores.id3tree?  &limit=3 &users =[X,Y,Z] output==> [1,2,3],[4,5,6],[7,8,9]
    # /api/v1/[ClientID]/predict/TopStores.forrest?  &limit=3 &users =[X,Y,Z] output==> [1,2,3],[4,5,6],[7,8,9]
    # /api/v1/[ClientID]/predict/TopStores.google?   &limit=3 &users =[X,Y,Z] output==> [1,2,3],[4,5,6],[7,8,9]



], debug=True)
