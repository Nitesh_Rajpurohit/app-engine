import logging
import webapp2

routes = webapp2.WSGIApplication([
    ('/worker/emailer', 'app.workers.emailer.SendEmail'),
    ('/worker/reporter', 'app.workers.reporter.MakeReportTab'),
    # will need to be be changed to controllers that kick off a task queue
    # attempting to stop app breakage via poor URL routing
], debug=True)