#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Nitesh
#
# Created:     26/06/2013
# Copyright:   (c) Nitesh 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

from google.appengine.ext import ndb

class TabsStatic(ndb.Model):
    TabName = ndb.StringProperty(required=True)
    TabId = ndb.StringProperty(required=True)
    Key = ndb.StringProperty(required=True)
    KeyValue = ndb.StringProperty(required=True)

class TabsDynamic(ndb.Model):
    TabName = ndb.StringProperty(required=True)
    TabId = ndb.StringProperty(required=True)
    ParamType = ndb.StringProperty(required=True)
    ExtAcc = ndb.StringProperty(required=True)
    ExtUserInput = ndb.StringProperty()



