#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Nitesh
#
# Created:     19/06/2013
# Copyright:   (c) Nitesh 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import MySQLdb
import sys
from datetime import date, timedelta

#-------------------------------------------------------------------------------
def query_wk1(cur,client_id,start,end):
    # File: Query
    # Number of promos, Views, Unique Views, Redemptions in a Week

    # set interval for different time period
    interval_3 = end-timedelta(days=21)
    interval_4 = end-timedelta(days=28)

    # Week 1
    select_record ="""SELECT count(distinct(promo_id)) As PromoCount
		              ,count(consumer_identity) As Views
		              ,count(distinct(consumer_identity)) As UnqiueViews
		              ,count(case when consumer_offer_transaction_state="Presented" then 1 else null end) AS Barcode
	                   FROM km_analytics.consumer_offer_transaction
	                   where (client_id = %s)
                         and consumer_offer_transaction_date between '%s' and '%s'
                       group by promo_id""" % (client_id, interval_4, interval_3)

    # execute the SQL query using execute() method.
    cur.execute(select_record)

    # fetch all of the rows from the query
    rows = cur.fetchall ()

    PromoCount = 0
    Views = 0
    UniqueViews = 0
    Barcode = 0

    # Adding content of all rows and returning aggregated values
    for row in rows:
        PromoCount = PromoCount + row[0]
        Views = Views + row[1]
        UniqueViews = UniqueViews + row[2]
        Barcode = Barcode + row[3]

    return (PromoCount, Views, UniqueViews, Barcode)


#-------------------
def query_wk2(cur,client_id,start,end):
    # File: Query
    # Number of promos, Views, Unique Views, Redemptions in a Week

    # set interval for different time period
    interval_0 = end
    interval_1 = end-timedelta(days=7)
    interval_2 = end-timedelta(days=14)
    interval_3 = end-timedelta(days=21)
    interval_4 = end-timedelta(days=28)

    # Week 2
    select_record ="""SELECT count(distinct(promo_id)) As PromoCount
		              ,count(consumer_identity) As Views
		              ,count(distinct(consumer_id)) As UnqiueViews
		              ,count(case when consumer_offer_transaction_state="Presented" then 1 else null end) AS Barcode
	                   FROM km_analytics.consumer_offer_transaction
	                   where (client_id = %s)
                         and consumer_offer_transaction_date between '%s' and '%s'
                       group by promo_id """ % (client_id, interval_3, interval_2)

    # execute the SQL query using execute() method.
    cur.execute(select_record)

    # fetch all of the rows from the query
    rows = cur.fetchall ()

    PromoCount = 0
    Views = 0
    UniqueViews = 0
    Barcode = 0

    # Adding content of all rows and returning aggregated values
    for row in rows:
        PromoCount = PromoCount + row[0]
        Views = Views + row[1]
        UniqueViews = UniqueViews + row[2]
        Barcode = Barcode + row[3]

    return (PromoCount, Views, UniqueViews, Barcode)

#-------------------
def query_wk3(cur,client_id,start,end):
    # File: Query
    # Number of promos, Views, Unique Views, Redemptions in a Week

    # set interval for different time period
    interval_0 = end
    interval_1 = end-timedelta(days=7)
    interval_2 = end-timedelta(days=14)
    interval_3 = end-timedelta(days=21)
    interval_4 = end-timedelta(days=28)

    # Week 3
    select_record ="""SELECT count(distinct(promo_id)) As PromoCount
		              ,count(consumer_id) As Views
		              ,count(distinct(consumer_identity)) As UnqiueViews
		              ,count(case when consumer_offer_transaction_state="Presented" then 1 else null end) AS Barcode
	                   FROM km_analytics.consumer_offer_transaction
	                   where (client_id = %s)
                         and consumer_offer_transaction_date between '%s' and '%s'
                       group by promo_id """ % (client_id, interval_2, interval_1)

    # execute the SQL query using execute() method.
    cur.execute(select_record)

    # fetch all of the rows from the query
    rows = cur.fetchall ()

    PromoCount = 0
    Views = 0
    UniqueViews = 0
    Barcode = 0

    # Adding content of all rows and returning aggregated values
    for row in rows:
        PromoCount = PromoCount + row[0]
        Views = Views + row[1]
        UniqueViews = UniqueViews + row[2]
        Barcode = Barcode + row[3]

    return (PromoCount, Views, UniqueViews, Barcode)

#-------------------

def query_wk4(cur,client_id,start,end):
    # File: Query
    # Number of promos, Views, Unique Views, Redemptions in a Week

    # set interval for different time period
    interval_0 = end
    interval_1 = end-timedelta(days=7)
    interval_2 = end-timedelta(days=14)
    interval_3 = end-timedelta(days=21)
    interval_4 = end-timedelta(days=28)

    # Week 4
    select_record ="""SELECT count(distinct(promo_id)) As PromoCount
		              ,count(consumer_identity) As Views
		              ,count(distinct(consumer_identity)) As UnqiueViews
		              ,count(case when consumer_offer_transaction_state="Presented" then 1 else null end) AS Barcode
	                   FROM km_analytics.consumer_offer_transaction
	                   where (client_id = %s)
                         and consumer_offer_transaction_date between '%s' and '%s'
                       group by promo_id """ % (client_id, interval_1, interval_0)

    # execute the SQL query using execute() method.
    cur.execute(select_record)


    # fetch all of the rows from the query
    rows = cur.fetchall ()

    PromoCount = 0
    Views = 0
    UniqueViews = 0
    Barcode = 0

    # Adding content of all rows and returning aggregated values
    for row in rows:
        PromoCount = PromoCount + row[0]
        Views = Views + row[1]
        UniqueViews = UniqueViews + row[2]
        Barcode = Barcode + row[3]

    return (PromoCount, Views, UniqueViews, Barcode)

#-------------------------------------------------------------------------------

def query_per_week_0(cur,client_id,start,end):

    # File: Query 6
    # Existing users as per that week

    # set interval for different time period
    interval_0 = end
    interval_1 = end-timedelta(days=7)
    interval_2 = end-timedelta(days=14)
    interval_3 = end-timedelta(days=21)
    interval_4 = end-timedelta(days=28)

    # Week 0
    select_record ="""SELECT count(distinct(consumer_identity)) AS Users
	                   FROM km_analytics.consumer_offer_transaction
	                   where (client_id = %s)
                         and consumer_offer_transaction_date <= '%s' """ % (client_id, interval_4)

    # execute the SQL query using execute() method.
    cur.execute(select_record)


    # fetch all of the rows from the query
    rows = cur.fetchall ()

    Users = 0

    # Adding content of all rows and returning aggregated values
    for row in rows:
        Users = Users + row[0]

    return (Users)

#-------------------

def query_per_week_1(cur,client_id,start,end):

    # File: Query 6
    # Existing users as per that week

    # set interval for different time period
    interval_0 = end
    interval_1 = end-timedelta(days=7)
    interval_2 = end-timedelta(days=14)
    interval_3 = end-timedelta(days=21)
    interval_4 = end-timedelta(days=28)

    # Week 1
    select_record ="""SELECT count(distinct(consumer_identity)) AS Users
	                   FROM km_analytics.consumer_offer_transaction
	                   where (client_id = %s)
                         and consumer_offer_transaction_date <= '%s' """ % (client_id, interval_3)

    # execute the SQL query using execute() method.
    cur.execute(select_record)


    # fetch all of the rows from the query
    rows = cur.fetchall ()

    Users = 0

    # Adding content of all rows and returning aggregated values
    for row in rows:
        Users = Users + row[0]

    return (Users)

#-------------------

def query_per_week_2(cur,client_id,start,end):

    # File: Query 6
    # Existing users as per that week

    # set interval for different time period
    interval_0 = end
    interval_1 = end-timedelta(days=7)
    interval_2 = end-timedelta(days=14)
    interval_3 = end-timedelta(days=21)
    interval_4 = end-timedelta(days=28)

    # Week 2
    select_record ="""SELECT count(distinct(consumer_identity)) AS Users
	                   FROM km_analytics.consumer_offer_transaction
	                   where (client_id = %s)
                         and consumer_offer_transaction_date <= '%s' """ % (client_id, interval_2)

    # execute the SQL query using execute() method.
    cur.execute(select_record)


    # fetch all of the rows from the query
    rows = cur.fetchall ()

    Users = 0

    # Adding content of all rows and returning aggregated values
    for row in rows:
        Users = Users + row[0]

    return (Users)

#-------------------

def query_per_week_3(cur,client_id,start,end):

    # File: Query 6
    # Existing users as per that week

    # set interval for different time period
    interval_0 = end
    interval_1 = end-timedelta(days=7)
    interval_2 = end-timedelta(days=14)
    interval_3 = end-timedelta(days=21)
    interval_4 = end-timedelta(days=28)

    # Week 3
    select_record ="""SELECT count(distinct(consumer_identity)) AS Users
	                   FROM km_analytics.consumer_offer_transaction
	                   where (client_id = %s)
                         and consumer_offer_transaction_date <= '%s' """ % (client_id, interval_1)

    # execute the SQL query using execute() method.
    cur.execute(select_record)


    # fetch all of the rows from the query
    rows = cur.fetchall ()

    Users = 0

    # Adding content of all rows and returning aggregated values
    for row in rows:
        Users = Users + row[0]

    return (Users)

#-------------------

def query_per_week_4(cur,client_id,start,end):

    # File: Query 6
    # Existing users as per that week

    # set interval for different time period
    interval_0 = end
    interval_1 = end-timedelta(days=7)
    interval_2 = end-timedelta(days=14)
    interval_3 = end-timedelta(days=21)
    interval_4 = end-timedelta(days=28)

    # Week 4
    select_record ="""SELECT count(distinct(consumer_identity)) AS Users
	                   FROM km_analytics.consumer_offer_transaction
	                   where (client_id = %s)
                         and consumer_offer_transaction_date <= '%s' """ % (client_id, interval_0)

    # execute the SQL query using execute() method.
    cur.execute(select_record)


    # fetch all of the rows from the query
    rows = cur.fetchall ()

    Users = 0

    # Adding content of all rows and returning aggregated values
    for row in rows:
        Users = Users + row[0]

    return (Users)


#-------------------------------------------------------------------------------

def query_usr_redeem_4(cur,client_id,start,end):

    # File: Query 7
    # Existing User Redemptions

    # set interval for different time period
    interval_0 = end
    interval_1 = end-timedelta(days=7)
    interval_2 = end-timedelta(days=14)
    interval_3 = end-timedelta(days=21)
    interval_4 = end-timedelta(days=28)

    # Week 4
    select_record ="""SELECT count(distinct(consumer_identity)) As ExistingUserRedemptions
	                   FROM km_analytics.consumer_offer_transaction
	                   where (client_id = %s)
                         and consumer_offer_transaction_state="Presented"
	                     and consumer_offer_transaction_date between '%s'	and '%s'
                         and (consumer_identity) in
	                                       (SELECT consumer_identity
	                                           FROM km_analytics.consumer_offer_transaction
	                                           where (client_id = %s )
	                                             and consumer_offer_transaction_date <= '%s') """ % (client_id,interval_1,interval_0,client_id,interval_1)

    # execute the SQL query using execute() method.
    print select_record
    cur.execute(select_record)


    # fetch all of the rows from the query
    rows = cur.fetchall ()

    ExistingUserRedemptions = 0

    # Adding content of all rows and returning aggregated values
    for row in rows:
        ExistingUserRedemptions = ExistingUserRedemptions + row[0]

    return (ExistingUserRedemptions)

#-------------------

def query_usr_redeem_3(cur,client_id,start,end):

    # File: Query 7
    # Existing User Redemptions

    # set interval for different time period
    interval_0 = end
    interval_1 = end-timedelta(days=7)
    interval_2 = end-timedelta(days=14)
    interval_3 = end-timedelta(days=21)
    interval_4 = end-timedelta(days=28)

    # Week 3
    select_record ="""SELECT count(distinct(consumer_identity)) As ExistingUserRedemptions
	                   FROM km_analytics.consumer_offer_transaction
	                   where (client_id = %s)
                         and consumer_offer_transaction_state="Presented"
	                     and consumer_offer_transaction_date between '%s'	and '%s'
                         and (consumer_identity) in
	                                       (SELECT consumer_identity
	                                           FROM km_analytics.consumer_offer_transaction
	                                           where (client_id = %s )
	                                             and consumer_offer_transaction_date <= '%s') """ % (client_id,interval_2,interval_1,client_id,interval_2)

    # execute the SQL query using execute() method.
    cur.execute(select_record)


    # fetch all of the rows from the query
    rows = cur.fetchall ()

    ExistingUserRedemptions = 0

    # Adding content of all rows and returning aggregated values
    for row in rows:
        ExistingUserRedemptions = ExistingUserRedemptions + row[0]

    return (ExistingUserRedemptions)

#-------------------

def query_usr_redeem_2(cur,client_id,start,end):

    # File: Query 7
    # Existing User Redemptions

    # set interval for different time period
    interval_0 = end
    interval_1 = end-timedelta(days=7)
    interval_2 = end-timedelta(days=14)
    interval_3 = end-timedelta(days=21)
    interval_4 = end-timedelta(days=28)

    # Week 2
    select_record ="""SELECT count(distinct(consumer_identity)) As ExistingUserRedemptions
	                   FROM km_analytics.consumer_offer_transaction
	                   where (client_id = %s)
                         and consumer_offer_transaction_state="Presented"
	                     and consumer_offer_transaction_date between '%s'	and '%s'
                         and (consumer_identity) in
	                                       (SELECT consumer_identity
	                                           FROM km_analytics.consumer_offer_transaction
	                                           where (client_id = %s)
	                                             and consumer_offer_transaction_date <= '%s') """ % (client_id,interval_3,interval_2,client_id,interval_3)

    # execute the SQL query using execute() method.
    cur.execute(select_record)


    # fetch all of the rows from the query
    rows = cur.fetchall ()

    ExistingUserRedemptions = 0

    # Adding content of all rows and returning aggregated values
    for row in rows:
        ExistingUserRedemptions = ExistingUserRedemptions + row[0]

    return (ExistingUserRedemptions)

#-------------------

def query_usr_redeem_1(cur,client_id,start,end):

    # File: Query 7
    # Existing User Redemptions

    # set interval for different time period
    interval_0 = end
    interval_1 = end-timedelta(days=7)
    interval_2 = end-timedelta(days=14)
    interval_3 = end-timedelta(days=21)
    interval_4 = end-timedelta(days=28)

    # Week 1
    select_record ="""SELECT count(distinct(consumer_identity)) As ExistingUserRedemptions
	                   FROM km_analytics.consumer_offer_transaction
	                   where (client_id = %s)
                         and consumer_offer_transaction_state="Presented"
	                     and consumer_offer_transaction_date between '%s'	and '%s'
                         and (consumer_identity) in
	                                       (SELECT consumer_identity
	                                           FROM km_analytics.consumer_offer_transaction
	                                           where (client_id = %s)
	                                             and consumer_offer_transaction_date <= '%s') """ % (client_id,interval_4,interval_3,client_id,interval_4)

    # execute the SQL query using execute() method.
    cur.execute(select_record)


    # fetch all of the rows from the query
    rows = cur.fetchall ()

    ExistingUserRedemptions = 0

    # Adding content of all rows and returning aggregated values
    for row in rows:
        ExistingUserRedemptions = ExistingUserRedemptions + row[0]

    return (ExistingUserRedemptions)


#-------------------------------------------------------------------------------

def query_cons_usr_redeem_4(cur,client_id,start,end):

    # File: Query from mcomm_platform3
    # Query: Consecutive Users Redemptions per week

    # set interval for different time period
    interval_0 = end
    interval_1 = end-timedelta(days=7)
    interval_2 = end-timedelta(days=14)
    interval_3 = end-timedelta(days=21)
    interval_4 = end-timedelta(days=28)

    # 4 out of 4
    select_record ="""SELECT count(*) AS InARow from(
                        SELECT count(*) AS CONSEC from
                            (SELECT distinct(consumer_identity)
                            	FROM km_analytics.consumer_offer_transaction
                            	where (client_id = %s)
                            	and consumer_offer_transaction_state="Presented"
                            	and consumer_offer_transaction_date
                            	between '%s'
                            	and '%s'
                            union all
                            SELECT distinct(consumer_identity)
                            	FROM km_analytics.consumer_offer_transaction
                            	where (client_id = %s)
                            	and consumer_offer_transaction_state="Presented"
                            	and consumer_offer_transaction_date
                            	between '%s'
                            	and '%s'
                            union all
                            SELECT distinct(consumer_identity)
                            	FROM km_analytics.consumer_offer_transaction
                            	where (client_id = %s)
                            	and consumer_offer_transaction_state="Presented"
                            	and consumer_offer_transaction_date
                            	between '%s'
                            	and '%s'
                            union all
                            SELECT distinct(consumer_identity)
                            	FROM km_analytics.consumer_offer_transaction
                            	where (client_id = %s)
                            	and consumer_offer_transaction_state="Presented"
                            	and consumer_offer_transaction_date
                            	between '%s'
                            	and '%s'
                            )INROW
                            group by consumer_identity having (count(consumer_identity) = 4)
                            )ConsCounter """ % (client_id,interval_1,interval_0,client_id,interval_2,interval_1,client_id,interval_3,interval_2,client_id,interval_4,interval_3)

    # execute the SQL query using execute() method.
    cur.execute(select_record)


    # fetch all of the rows from the query
    query_user_per_week_4 = cur.fetchall ()
    return query_user_per_week_4


#-------------------

def query_cons_usr_redeem_3(cur,client_id,start,end):

    # File: Query from mcomm_platform3
    # Query: Consecutive Users Redemptions per week

    # set interval for different time period
    interval_0 = end
    interval_1 = end-timedelta(days=7)
    interval_2 = end-timedelta(days=14)
    interval_3 = end-timedelta(days=21)
    interval_4 = end-timedelta(days=28)

    # 3 out of 4
    select_record ="""SELECT count(*) AS InARow from(
                        SELECT count(*) AS CONSEC from
                            (SELECT distinct(consumer_identity)
                            	FROM km_analytics.consumer_offer_transaction
                            	where (client_id = %s)
                            	and consumer_offer_transaction_state="Presented"
                            	and consumer_offer_transaction_date
                            	between '%s'
                            	and '%s'
                            union all
                            SELECT distinct(consumer_identity)
                            	FROM km_analytics.consumer_offer_transaction
                            	where (client_id = %s)
                            	and consumer_offer_transaction_state="Presented"
                            	and consumer_offer_transaction_date
                            	between '%s'
                            	and '%s'
                            union all
                            SELECT distinct(consumer_identity)
                            	FROM km_analytics.consumer_offer_transaction
                            	where (client_id = %s)
                            	and consumer_offer_transaction_state="Presented"
                            	and consumer_offer_transaction_date
                            	between '%s'
                            	and '%s'
                            union all
                            SELECT distinct(consumer_identity)
                            	FROM km_analytics.consumer_offer_transaction
                            	where (client_id = %s)
                            	and consumer_offer_transaction_state="Presented"
                            	and consumer_offer_transaction_date
                            	between '%s'
                            	and '%s'
                            )INROW
                            group by consumer_identity having (count(consumer_identity) = 3)
                            )ConsCounter """ % (client_id,interval_1,interval_0,client_id,interval_2,interval_1,client_id,interval_3,interval_2,client_id,interval_4,interval_3)

    # execute the SQL query using execute() method.
    cur.execute(select_record)


    # fetch all of the rows from the query
    query_user_per_week_3 = cur.fetchall ()
    return query_user_per_week_3


#-------------------

def query_cons_usr_redeem_2(cur,client_id,start,end):

    # File: Query from mcomm_platform3
    # Query: Consecutive Users Redemptions per week

    # set interval for different time period
    interval_0 = end
    interval_1 = end-timedelta(days=7)
    interval_2 = end-timedelta(days=14)
    interval_3 = end-timedelta(days=21)
    interval_4 = end-timedelta(days=28)

    # 2 out of 4
    select_record ="""SELECT count(*) AS InARow from(
                        SELECT count(*) AS CONSEC from
                            (SELECT distinct(consumer_identity)
                            	FROM km_analytics.consumer_offer_transaction
                            	where (client_id = %s)
                            	and consumer_offer_transaction_state="Presented"
                            	and consumer_offer_transaction_date
                            	between '%s'
                            	and '%s'
                            union all
                            SELECT distinct(consumer_identity)
                            	FROM km_analytics.consumer_offer_transaction
                            	where (client_id = %s)
                            	and consumer_offer_transaction_state="Presented"
                            	and consumer_offer_transaction_date
                            	between '%s'
                            	and '%s'
                            union all
                            SELECT distinct(consumer_identity)
                            	FROM km_analytics.consumer_offer_transaction
                            	where (client_id = %s)
                            	and consumer_offer_transaction_state="Presented"
                            	and consumer_offer_transaction_date
                            	between '%s'
                            	and '%s'
                            union all
                            SELECT distinct(consumer_identity)
                            	FROM km_analytics.consumer_offer_transaction
                            	where (client_id = %s)
                            	and consumer_offer_transaction_state="Presented"
                            	and consumer_offer_transaction_date
                            	between '%s'
                            	and '%s'
                            )INROW
                            group by consumer_identity having (count(consumer_identity) = 2)
                            )ConsCounter """ % (client_id,interval_1,interval_0,client_id,interval_2,interval_1,client_id,interval_3,interval_2,client_id,interval_4,interval_3)

    # execute the SQL query using execute() method.
    cur.execute(select_record)


    # fetch all of the rows from the query
    query_user_per_week_2 = cur.fetchall ()
    return query_user_per_week_2


#-------------------

def query_cons_usr_redeem_1(cur,client_id,start,end):

    # File: Query from mcomm_platform3
    # Query: Consecutive Users Redemptions per week

    # set interval for different time period
    interval_0 = end
    interval_1 = end-timedelta(days=7)
    interval_2 = end-timedelta(days=14)
    interval_3 = end-timedelta(days=21)
    interval_4 = end-timedelta(days=28)

    # 1 out of 4
    select_record ="""SELECT count(*) AS InARow from(
                        SELECT count(*) AS CONSEC from
                            (SELECT distinct(consumer_identity)
                            	FROM km_analytics.consumer_offer_transaction
                            	where (client_id = %s)
                            	and consumer_offer_transaction_state="Presented"
                            	and consumer_offer_transaction_date
                            	between '%s'
                            	and '%s'
                            union all
                            SELECT distinct(consumer_identity)
                            	FROM km_analytics.consumer_offer_transaction
                            	where (client_id = %s)
                            	and consumer_offer_transaction_state="Presented"
                            	and consumer_offer_transaction_date
                            	between '%s'
                            	and '%s'
                            union all
                            SELECT distinct(consumer_identity)
                            	FROM km_analytics.consumer_offer_transaction
                            	where (client_id = %s)
                            	and consumer_offer_transaction_state="Presented"
                            	and consumer_offer_transaction_date
                            	between '%s'
                            	and '%s'
                            union all
                            SELECT distinct(consumer_identity)
                            	FROM km_analytics.consumer_offer_transaction
                            	where (client_id = %s)
                            	and consumer_offer_transaction_state="Presented"
                            	and consumer_offer_transaction_date
                            	between '%s'
                            	and '%s'
                            )INROW
                            group by consumer_identity having (count(consumer_identity) = 1)
                            )ConsCounter """ % (client_id,interval_1,interval_0,client_id,interval_2,interval_1,client_id,interval_3,interval_2,client_id,interval_4,interval_3)

    # execute the SQL query using execute() method.
    cur.execute(select_record)


    # fetch all of the rows from the query
    query_user_per_week_1 = cur.fetchall ()
    return query_user_per_week_1



#-------------------------------------------------------------------------------

def query_act_users_1(cur,client_id,start,end):
    # Active Users in a week

    # set interval for different time period
    interval_0 = end
    interval_1 = end-timedelta(days=7)
    interval_2 = end-timedelta(days=14)
    interval_3 = end-timedelta(days=21)
    interval_4 = end-timedelta(days=28)

    # Week 1
    select_record ="""SELECT count(distinct(consumer_identity)) AS Users
	                   FROM km_analytics.consumer_offer_transaction
	                   where (client_id = %s)
                         and consumer_offer_transaction_date between '%s' and '%s' """ % (client_id, interval_4,interval_3)

    # execute the SQL query using execute() method.
    cur.execute(select_record)

    # fetch all of the rows from the query
    rows = cur.fetchall ()

    Users = 0

    # Adding content of all rows and returning aggregated values
    for row in rows:
        Users = Users + row[0]

    return (Users)

#-------------------

def query_act_users_2(cur,client_id,start,end):
    # Active Users in a week

    # set interval for different time period
    interval_0 = end
    interval_1 = end-timedelta(days=7)
    interval_2 = end-timedelta(days=14)
    interval_3 = end-timedelta(days=21)
    interval_4 = end-timedelta(days=28)

    # Week 2
    select_record ="""SELECT count(distinct(consumer_identity)) AS Users
	                   FROM km_analytics.consumer_offer_transaction
	                   where (client_id = %s)
                         and consumer_offer_transaction_date between '%s' and '%s' """ % (client_id, interval_3,interval_2)

    # execute the SQL query using execute() method.
    cur.execute(select_record)

    # fetch all of the rows from the query
    rows = cur.fetchall ()

    Users = 0

    # Adding content of all rows and returning aggregated values
    for row in rows:
        Users = Users + row[0]

    return (Users)

#-------------------

def query_act_users_3(cur,client_id,start,end):
    # Active Users in a week

    # set interval for different time period
    interval_0 = end
    interval_1 = end-timedelta(days=7)
    interval_2 = end-timedelta(days=14)
    interval_3 = end-timedelta(days=21)
    interval_4 = end-timedelta(days=28)

    # Week 3
    select_record ="""SELECT count(distinct(consumer_identity)) AS Users
	                   FROM km_analytics.consumer_offer_transaction
	                   where (client_id = %s)
                         and consumer_offer_transaction_date between '%s' and '%s' """ % (client_id, interval_2,interval_1)

    # execute the SQL query using execute() method.
    cur.execute(select_record)

    # fetch all of the rows from the query
    rows = cur.fetchall ()

    Users = 0

    # Adding content of all rows and returning aggregated values
    for row in rows:
        Users = Users + row[0]

    return (Users)

#-------------------

def query_act_users_4(cur,client_id,start,end):
    # Active Users in a week

    # set interval for different time period
    interval_0 = end
    interval_1 = end-timedelta(days=7)
    interval_2 = end-timedelta(days=14)
    interval_3 = end-timedelta(days=21)
    interval_4 = end-timedelta(days=28)

    # Week 4
    select_record ="""SELECT count(distinct(consumer_identity)) AS Users
	                   FROM km_analytics.consumer_offer_transaction
	                   where (client_id = %s)
                         and consumer_offer_transaction_date between '%s' and '%s' """ % (client_id, interval_1,interval_0)

    # execute the SQL query using execute() method.
    cur.execute(select_record)

    # fetch all of the rows from the query
    rows = cur.fetchall ()

    Users = 0

    # Adding content of all rows and returning aggregated values
    for row in rows:
        Users = Users + row[0]

    return (Users)
#-------------------------------------------------------------------------------
