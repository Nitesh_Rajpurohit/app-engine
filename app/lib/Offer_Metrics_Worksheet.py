#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Nitesh
#
# Created:     19/06/2013
# Copyright:   (c) Nitesh 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import MySQLdb
import sys
import datetime
import time
def Offer(cur,client_id,start,end):
        # File: Offer
        # To Extract the Offer Query Data

    select_record ="""SELECT	 t3.promo_id
                		,t3.client_id
                		,t3.offer_transaction_dp_name
                		,t3.offer_transaction_date
                		,t4.offer_transaction_date
                		,t1.consumer_offer_transaction_source
                		,t1.promo_id
                		,count(t1.consumer_id) AS Views
                		,count(distinct t1.consumer_id) AS UniqueViews
                		,count(case when t1.consumer_offer_transaction_state="Presented" then 1 else null end) AS Barcode
                		,AVG(t1.consumer_offer_transaction_date - t2.consumer_offer_transaction_date) AS TimeToRedeem
                	FROM
                    	consumer_offer_transaction t1
                    	Inner Join
                    	consumer_offer_transaction t2
                    	on t1.consumer_id = t2.consumer_id
                    	Left Join (
                    	offer_transaction t3
                    	Inner Join
                    	offer_transaction t4 on t3.promo_id = t4.promo_id
                    	)
                    	on t1.promo_id = t3.promo_id
                	WHERE
                    	(t1.client_id in '%s')
                    	and
                    	t3.offer_transaction_state = "Started"
                    	and
                    	t4.offer_transaction_state = "Expired"
                    	and
                    	t1.consumer_offer_transaction_state="Presented"
                    	and
                    	t2.consumer_offer_transaction_state="Viewed"
                    	Group By
                    	t3.promo_id
                    	,t3.client_id
                    	,t1.promo_id
                    	,t3.offer_transaction_dp_name""" % (client_id)

        # execute the SQL query using execute() method.
    cur.execute(select_record)


        # fetch all of the rows from the query
    query_offer_data = cur.fetchall ()
    return query_offer_data


#-------------------------------------------------------------------------------
def benchmark_all(cur,client_id,start,end):
        # File: BenchmarkFinal
        # Query: To extract the Benchmark Metrics Data
        # All Time
    select_record ="""SELECT
                    	 count(distinct(t1.consumer_id)) AS UniqueViews
                    	,count(case when t1.consumer_offer_transaction_state="Redeemed" then 1 else null end) AS Barcode
                        ,(count(case when t1.consumer_offer_transaction_state="Redeemed" then 1 else null end)/count(distinct(t1.consumer_id)))*100
                        ,count(t1.consumer_id) / count(distinct(t1.consumer_id))
                    	,AVG((t1.consumer_offer_transaction_date-t2.consumer_offer_transaction_date)) AS TimeTaken
                    	FROM
                        	consumer_offer_transaction t1
                        	Inner Join
                        	consumer_offer_transaction t2
                        	on (t1.consumer_id = t2.consumer_id)
                    	where t1.consumer_offer_transaction_state = "Redeemed"
                    	  and t2.consumer_offer_transaction_state = "viewed"
                          and t1.client_id = %s """ % (client_id)

        # execute the SQL query using execute() method.
    cur.execute(select_record)


        # fetch all of the rows from the query
    query_bechmark_final_all_time = cur.fetchall ()
    return query_bechmark_final_all_time

#----------------
def benchmark_ytd(cur,client_id,start,end):
        # Year To Date
    select_record ="""SELECT
                    	 count(distinct(t1.consumer_id)) AS UniqueViews
                    	,count(case when t1.consumer_offer_transaction_state="Redeemed" then 1 else null end) AS Barcode
                        ,(count(case when t1.consumer_offer_transaction_state="Redeemed" then 1 else null end)/count(distinct(t1.consumer_id)))*100
                        ,count(t1.consumer_id) / count(distinct(t1.consumer_id))
                    	,AVG((t1.consumer_offer_transaction_date-t2.consumer_offer_transaction_date)) AS TimeTaken
                    	FROM
                        	consumer_offer_transaction t1
                        	Inner Join
                        	consumer_offer_transaction t2
                        	on (t1.consumer_id = t2.consumer_id)
                    	where t1.consumer_offer_transaction_state = "Redeemed"
                    	  and t2.consumer_offer_transaction_state = "viewed"
                          and t1.client_id = %s
                          and t1.consumer_offer_transaction_date between '%s' and '%s' """ % (client_id,'2013-01-01',end)

        # execute the SQL query using execute() method.
    cur.execute(select_record)


        # fetch all of the rows from the query
    query_bechmark_final_yr_2_date = cur.fetchall ()
    return query_bechmark_final_yr_2_date

#----------------
def benchmark_week(cur,client_id,start,end):
        # Weekly
    select_record ="""SELECT
                    	 count(distinct(t1.consumer_id)) AS UniqueViews
                    	,count(case when t1.consumer_offer_transaction_state="Redeemed" then 1 else null end) AS Barcode
                        ,(count(case when t1.consumer_offer_transaction_state="Redeemed" then 1 else null end)/count(distinct(t1.consumer_id)))*100
                        ,count(t1.consumer_id) / count(distinct(t1.consumer_id))
                    	,AVG((t1.consumer_offer_transaction_date-t2.consumer_offer_transaction_date)) AS TimeTaken
                    	FROM
                        	consumer_offer_transaction t1
                        	Inner Join
                        	consumer_offer_transaction t2
                        	on (t1.consumer_id = t2.consumer_id)
                    	where t1.consumer_offer_transaction_state = "Redeemed"
                    	  and t2.consumer_offer_transaction_state = "viewed"
                          and t1.client_id = %s
                          and t1.consumer_offer_transaction_date between '%s' and '%s' """ % (client_id,start,end)

        # execute the SQL query using execute() method.
    cur.execute(select_record)


        # fetch all of the rows from the query
    query_bechmark_final_weekly = cur.fetchall ()
    return query_bechmark_final_weekly


#-------------------------------------------------------------------------------

