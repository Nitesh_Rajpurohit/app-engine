import logging
import webapp2
import jinja2
import os

from google.appengine.ext import ndb
from google.appengine.api import users
from google.appengine.api.mail import EmailMessage


def guestbook_key(guestbook_name='default_guestbook'):
    return ndb.Key('Guestbook', guestbook_name)

jinja2_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'])

class SendEmail(webapp2.RequestHandler):
    def get(self):
        if users.get_current_user():
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        template = jinja2_environment.get_template('Sample_Email.html')
        self.response.out.write(template.render(
                                                url=url,
                                                url_linktext=url_linktext))


    def post(self): # should run at most 1/s
        msg = EmailMessage(sender=self.request.get('sender'),
                           to=self.request.get('to'),
                           subject = self.request.get('subject'),
                           body = self.request.get('body'),
                           attachments = (self.request.get('attachment'),
                           'application/vnd.ms-excel'))

        if self.request.get('CC') != '':
            msg.initialize(cc=self.request.get('CC'))

        msg.send()




    def txn():
        counter = Counter.get_by_key_name(key)
        if counter is None:
               counter = Counter(key_name=key, count=1)
        else:
               counter.count += 1
        counter.put()
        db.run_in_transaction(txn)