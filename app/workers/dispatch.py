import jinja2
import os
import webapp2
import logging
import calendar

from google.appengine.ext import ndb
from google.appengine.api import users

from ..models.greeting import Greeting as Geeting
from ..models.reports import Report as Reports

def guestbook_key(guestbook_name='default_guestbook'):
    return ndb.Key('Guestbook', guestbook_name)


jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'])

class WeeklyDispatcher(webapp2.RequestHandler):
    def GET(self):

        # Look up the current server time
        # perform off set if needed
        
        # Look up reports table to see what needs to be run
        reports = Reports.query(ScheduleType='weekly', ScheduleValue=datetime.datetime.now().weekday())

        # Loop through record set and kick off new tasks to handle each report
        # with appro params passed in to the reporter
        for report in reports:
            taskqueue.add(url='/worker/reporter', 
                      params={'client': report.ClientID, #echo back the variable inbox
                              'client': report.ClientID, #echo back the variable inbox
                              })

        


class MonthlyDispatcher(webapp2.RequestHandler):
    def GET(self):

        # Look up the current server time
        # perform off set if needed
        
        # Look up reports table to see what needs to be run
        reports = Reports.query(ScheduleType='monthly', ScheduleValue=datetime.datetime.now().day())

        # Loop through record set and kick off new tasks to handle each report
        # with appro params passed in to the reporter
        for report in reports:
            taskqueue.add(url='/worker/reporter', 
                      params={'client': report.ClientID, #echo back the variable inbox
                              'client': report.ClientID, #echo back the variable inbox
                              })

