#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Nitesh
#
# Created:     11/07/2013
# Copyright:   (c) Nitesh 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import sys
sys.path.insert(0, 'ext_lib')

import jinja2
import os
import webapp2
import logging
import calendar
import datetime
import csv
import urllib
from dateutil import parser

from google.appengine.api import taskqueue
from google.appengine.ext import ndb
from google.appengine.api import users
import xlwt
import jsonpickle
import requests

import analytics_api_v3_auth
from apiclient.errors import HttpError
from oauth2client.client import AccessTokenRefreshError

from google.appengine.api import urlfetch


def guestbook_key(guestbook_name='default_guestbook'):
    return ndb.Key('Guestbook', guestbook_name)
#-------------------------------------------------------------------------------
def get_results(service, start_S, end_S):
    #setting the profile ID
    profile_id = '53021791'

    # Use the Analytics Service Object to query the Core Reporting API
    return service.data().ga().get(
      ids='ga:' + profile_id,
      start_date=start_S,
      end_date=end_S,
      metrics='ga:uniquePageviews',
      filters='ga:pagePath=~^/coupon*').execute()

#-------------------------------------------------------------------------------
def csv_data(start_S, end_S):
    # Downloading the CSV File
    url = "http://dash.ops.kouponmedia.com/michaels_sms_count.csv"
    webpage = urllib.urlopen(url)
    datareader = csv.reader(webpage, dialect='excel-tab')
    data = []
    for row in datareader:
        data.append(row)

    # Changing  format of Date
    start_new = parser.parse(start_S)
    end_new = parser.parse(end_S)
    log_count = 0

    # Looping on all the records of CSV file and caluculating total log_count in desired period
    for row in data :
            dt = parser.parse(row[0])
            # Find Diffrence between current date and Given period
            diff1 = dt - start_new
            diff2 = dt - end_new
            # If current date is greater than start date and less than end date, ADD the value of SMS count to log Count
            if (diff1 >= datetime.timedelta(seconds=0)) and (diff2 <= datetime.timedelta(seconds=0)):
                log_count = log_count + int(row[1])

    return log_count

#-------------------------------------------------------------------------------

def sql_data(start_S, end_S,rpc):

    # Checking If the URL Fetch service is ready or not, If Yes fetch the data else Inform user saying Server is down
    try:
        result = rpc.get_result()
        if result.status_code == 200:
            text = result.content

            # Calling Herokuapp for executing the MySql Query
            url = "http://sequelizer.herokuapp.com/sql"

            # Passing query for "Michaels DT Unique Viewed" as an parameter to Herokuapp
            records_1 = """ SELECT count(distinct(consumer_identity))
            	               FROM consumer_offer_transaction
            	               where client_id= 1
            	               and consumer_offer_transaction_source='desktop'
            	               and consumer_offer_transaction_state='Viewed'
                               and consumer_offer_transaction_date between '%s' and '%s' """ % (start_S, end_S)
            data_1 = {'query': records_1}
            headers = {}
            query_1 = requests.post(url,data = data_1, headers = headers)
            query = jsonpickle.decode(query_1.text)

            sql_1_data =  query[0]["count(distinct(consumer_identity))"]


            # Passing query for "Michaels DT Unique Redeemed" as an parameter to Herokuapp
            records_2 = """ SELECT count(distinct(consumer_identity))
        	               FROM consumer_offer_transaction
        	               where client_id = 1
        	               and consumer_offer_transaction_source='desktop'
                           and consumer_offer_transaction_state='Presented'
                           and consumer_offer_transaction_date between '%s' and '%s' """ % (start_S, end_S)
            data_2 = {'query': records_2}
            headers = {}
            query_2 = requests.post(url,data = data_2, headers = headers)
            query = jsonpickle.decode(query_2.text)

            sql_2_data =  query[0]["count(distinct(consumer_identity))"]

    except urlfetch.DownloadError:
        sql_1_data = 0
        sql_2_data = 0

    return (sql_1_data , sql_2_data)

#-------------------------------------------------------------------------------


jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'])


class Attachment(webapp2.RequestHandler):
    def GET(self):

        if users.get_current_user():
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        template = jinja2_environment.get_template('Sample_Email.html')
        self.response.out.write(template.render(
                                                url=url,
                                                url_linktext=url_linktext))

    def post(self):

        #step 0: Wake up the URL fetch Service
        rpc = urlfetch.create_rpc()
        urlfetch.make_fetch_call(rpc, "http://sequelizer.herokuapp.com/sql")

        #Step 1 Reading Start & End date of the period
        start = self.request.get('start')
        start_S = self.request.get('start_S')
        print start_S

        end = self.request.get('end')
        end_S = self.request.get('end_S')
        end_sql = self.request.get('end_sql')
        print end_S


        # Step 2 Geting the Data from Google Analystics Server
        # Step 2-1. Get an analytics service object.
        ga_data = 10000
        service = analytics_api_v3_auth.initialize_service()

        try:
                # Step 2-2. Query the Core Reporting API.
                results = get_results(service, start_S, end_S)

                # Step 2-3. Output the results.
                ga_data = results.get('rows')[0][0]

        except HttpError, error:
                # Handle API errors.
               print ('Arg, there was an API error : %s : %s' %
                       (error.resp.status, error._get_reason()))

        except AccessTokenRefreshError:
                # Handle Auth errors.
                print ('The credentials have been revoked or expired, please re-run the application to re-authorize')



        # Step 3. Getting data from log file
        results = csv_data(start_S, end_S)
        msg_log = results


        # Step 4. Getting the dat from the MS SQL Server.
        results = sql_data(start_S,end_sql,rpc)
        uni_viewed = results[0]
        uni_redeemed = results[1]

        print ga_data, msg_log, uni_viewed, uni_redeemed

        # Creating the excel sheet
        workbook = xlwt.Workbook()

        # Adding a worksheet to the WorkBook
        worksheet = workbook.add_sheet('My Worksheet')

        if ga_data == 0 or msg_log == 0 or uni_viewed == 0 or uni_redeemed == 0:
            worksheet.write(0, 0,'Report Server is Down.')
            worksheet.write(0, 1,'Sorry For the inconvenience.')
            worksheet.write(0, 2,'Please contact System Admin.')
        else :

            #Creating style templates for different cell formating
            pattern = xlwt.Pattern()
            pattern.pattern = xlwt.Pattern.SOLID_PATTERN
            pattern.pattern_fore_colour = 0x3A

            style1 = xlwt.easyxf('font: height 250, name Tahoma; align: wrap on, vert center, horiz center;')
            style2 = xlwt.easyxf('font: bold on; align: horiz right;')
            style3 = xlwt.easyxf('font: height 250;align: horiz right;')
            style4 = xlwt.easyxf('font: bold on;pattern: pattern solid;')
            style4.pattern.pattern_fore_colour = 76
            style5 = xlwt.easyxf('font: height 250;')

            #Setting column's width
            worksheet.col(0).width = 256*12
            worksheet.col(1).width = 256*14
            worksheet.col(2).width = 256*35
            worksheet.col(3).width = 256*14
            worksheet.col(4).width = 256*14
            worksheet.col(5).width = 256*14
            worksheet.col(6).width = 256*20


            #Writing the data into the sheet

            worksheet.write_merge(1, 7, 0, 0, 'Week',style1)
            worksheet.write(1, 1,'beginning:')
            worksheet.write(1, 2,'through end of:')
            worksheet.write(3, 2, ' ',style4)
            worksheet.write(3, 3,'Uniques',style4)
            worksheet.write(3, 4,'% of Total',style4)
            worksheet.write(3, 5,'% of sent',style4)
            worksheet.write(3, 6,'step conversions',style4)
            worksheet.write(4, 2,'Desktop WeeklyAd Coupon Views')
            worksheet.write(5, 2,'Coupon Links created for SMS')
            worksheet.write(6, 2,'Opened the SMS')
            worksheet.write(7, 2,'Saw a Barcode')

            worksheet.write(2, 1,start_S,style2)
            worksheet.write(2, 2,end_S,style2)

            worksheet.write(4, 3,int(ga_data),style5)
            worksheet.write(5, 3,msg_log,style5)
            worksheet.write(6, 3,uni_viewed,style5)
            worksheet.write(7, 3,uni_redeemed,style5)

            worksheet.write(4, 4,"{0:.0f}%".format(float(ga_data)/float(ga_data) * 100),style3)
            worksheet.write(5, 4,"{0:.0f}%".format(float(msg_log)/float(ga_data) * 100),style3)
            worksheet.write(6, 4,"{0:.0f}%".format(float(uni_viewed)/float(ga_data) * 100),style3)
            worksheet.write(7, 4,"{0:.0f}%".format(float(uni_redeemed)/float(ga_data) * 100),style3)

            worksheet.write(4, 5,'--')
            worksheet.write(5, 5,"{0:.0f}%".format(float(msg_log)/float(msg_log) * 100),style3)
            worksheet.write(6, 5,"{0:.0f}%".format(float(uni_redeemed)/float(msg_log) * 100),style3)
            worksheet.write(7, 5,"{0:.0f}%".format(float(uni_viewed)/float(msg_log) * 100),style3)

            worksheet.write(4, 6,'--')
            worksheet.write(5, 6,"{0:.0f}%".format(float(msg_log)/float(ga_data) * 100),style3)
            worksheet.write(6, 6,"{0:.0f}%".format(float(uni_viewed)/float(msg_log) * 100),style3)
            worksheet.write(7, 6,"{0:.0f}%".format(float(uni_redeemed)/float(uni_viewed) * 100),style3)

        # Saving the Excel workbook
        self.response.headers['Content-Type'] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        self.response.headers['Content-Transfer-Encoding'] = 'Binary'
        #self.response.headers['Content-Transfer-Encoding'] = 'utf-8'
        self.response.headers['Content-disposition'] = 'attachment; filename="Excel_Workbook.xls"'
        workbook.save(self.response.out)

        attachment = 'Excel_Workbook.xls'
        logging.info(attachment)
        #self.response.out.write(self.response.out)


        taskqueue.add(url='/worker/emailer',
            			  params={'sender': self.request.get('sender'),
            			  		  'to':self.request.get('to'),
            			  		  'cc':self.request.get('cc'),
                                  'subject': self.request.get('subject'),
                                  'body':self.request.get('body'),
                                  'attachment':attachment})


